package com.casestudy.login.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.casestudy.login.entity.Admin;
import java.lang.String;

public interface LoginRepository extends JpaRepository<Admin, Long> {
	
	Admin findByUserNameAndPassword(String username,String password);

}
