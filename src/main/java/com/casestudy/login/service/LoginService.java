package com.casestudy.login.service;

import com.casestudy.login.entity.Admin;

public interface LoginService {

	Admin getUser(Admin admin);
}
