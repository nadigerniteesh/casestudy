package com.casestudy.login.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.casestudy.login.entity.Admin;
import com.casestudy.login.repository.LoginRepository;

@Service
public class LoginServiceImpl implements LoginService {

	
	@Autowired
	LoginRepository repo;
	
	@Override
	public Admin getUser(Admin admin) {
		return repo.findByUserNameAndPassword(admin.getUserName(),admin.getPassword());
	}

}
