package com.casestudy.login.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.casestudy.login.entity.Admin;
import com.casestudy.login.service.LoginService;

@RestController
public class LoginController {
	
	@Autowired
	LoginService service;
	
	
	@PostMapping("/login")
	public Admin getUser(@RequestBody Admin admin) {
		return service.getUser(admin);
		
	}

}
